﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public int hp = 2;
    float reload = 1000;
    float faster = .1f;
    public GameObject rocket;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        //Kod na wystrzelenie rakiety
        if (Random.Range(0, reload) <= 1)
        {
            Instantiate(rocket, new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z), Quaternion.identity);
            reload -= faster;
        }
	
	}
    //Kod na zniszczenie przez czołg
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("tank") && hp < 1)
        {
            
            Destroy(this.gameObject);
                
        }
    }
}
